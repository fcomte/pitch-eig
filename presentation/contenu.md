<!-- .slide: class="slide" -->
# Le problème

Pour beaucoup trop de datascientists des ministères, le bonheur c'est d'avoir le droit d'utiliser un portable customisé

----

<!-- .slide: data-background-image="logo_Insee.png" data-background-position="center" data-background-opacity=0.2 data-background-size="cover"-->

<!-- .slide: class="slide" -->
# Le défi qu'on souhaite relever avec vous

Création d'une plateforme Datascience basée sur les technologies cloud (containerisation, stockage orienté objet..)

----

<!-- .slide: data-background-image="logo_Insee.png" data-background-position="center" data-background-opacity=0.2 data-background-size="cover"-->

<!-- .slide: class="slide" -->
# Deux versants à notre projet

- le [projet opensource](https://github.com/InseeFrLab/onyxia-ui)
- l'[instantiation de ce projet](https://spyrales.sspcloud.fr)  pour le service statistique publique

----

<!-- .slide: data-background-image="logo_Insee.png" data-background-position="center" data-background-opacity=0.2 data-background-size="cover"-->

<!-- .slide: class="slide" -->
# Pourquoi postuler ?

- intégrer une équipe de 4 personnes motivées
- progresser ensemble (design du service , développement du front JS, services data)
- une aventure en devenir et des opportunités peut-être pour vous.




